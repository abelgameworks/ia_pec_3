using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour
{
    public GameObject controls;
    public GameObject elders;
    public GameObject runners;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            runners.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            foreach (Transform elder in elders.transform)
            {
                elder.gameObject.SetActive(false);
            }
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            controls.SetActive(!controls.activeSelf);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SceneManager.LoadScene("Abel Pazo Plaza");
        }
    }
}
