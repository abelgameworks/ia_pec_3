using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThiefManager : MonoBehaviour
{
    public bool isThiefAlive = true;
    public GameObject Thief;

    public GameObject ThiefPrefab;
    public GameObject Policeman;

    public float timeForRespawn = 5f;

    private float timer = 0f;

    bool respawning;

    private void Start()
    {
        respawning = false;
    }

    private void Update()
    {
        if(respawning)
        {
            timer += Time.deltaTime;

            if(timer >= timeForRespawn)
            {
                Respawn();
            }
        }
    }

    public void Catch()
    {
        //TODO: Destroy the thief, wait for respawn and create a new one
        Destroy(Thief);
        isThiefAlive = false;
        respawning = true;

    }

    void Respawn()
    {
        Thief = Instantiate(ThiefPrefab, transform.position, Quaternion.identity);
        Thief.GetComponent<BehaviorExecutor>().SetBehaviorParam("PoliceTarget", Policeman);
        isThiefAlive = true;
        respawning = false;
    }
}
