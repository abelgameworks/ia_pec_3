using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneRefresh : MonoBehaviour
{
    private float timer = 0f;
    public float timeLimit = 60.0f;

    private void Awake()
    {
        timer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if(timer > timeLimit)
        {
            SceneManager.LoadScene(0);
        }
    }
}
