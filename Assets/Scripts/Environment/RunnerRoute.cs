using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class for storing the list of areas that compose the Runner Navigation Area
public class RunnerRoute : MonoBehaviour
{
    public List<Area> areas;
}