using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Stores the different wayward points defined for an area
public class Area : MonoBehaviour
{
    public List<GameObject> points; // List that stores all the empty gameobjects that work as points

    public GameObject getRandomPoint()
    {
        return points[Random.Range(0, points.Count - 1)]; // Selects one of the random points and returns it
    }
}