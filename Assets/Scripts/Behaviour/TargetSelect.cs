using UnityEngine;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using Pada1.BBCore.Framework;

[Action("MyActions/Policeman/TargetSelect")]
[Help("Finds a Thief to look out for")]
public class TargetSelect : BasePrimitiveAction
{
    [InParam("ThiefManager")]
    [Help("Reference to the Thief Manager GameObject")]
    public ThiefManager ThiefManager;

    [OutParam("TargetThief")]
    [Help("Thief currently targeted by this policeman")]
    public GameObject target;


    public override TaskStatus OnUpdate()
    {
        target = ThiefManager.Thief;

        return base.OnUpdate();
    }
}
