using Pada1.BBCore;
using Pada1.BBCore.Framework;
using Pada1.BBCore.Tasks;
using UnityEngine;


[Action("MyActions/Thief/ResetThiefParams")]
[Help("Resets the params used by the thief")]
public class ResetThiefParams : BasePrimitiveAction
{
	[InParam("closeEnough")]
	[Help("Is the target close enough to steal from him?")]
	private bool closeEnough;

	[InParam("targetFound")]
	[Help("Does the thief have a target?")]
	private bool targetFound;

	[InParam("StealTarget")]
	[Help("Target of the steal")]
	private GameObject StealTarget;

	public override TaskStatus OnUpdate()
	{

		closeEnough = false;
		targetFound = false;
		StealTarget = null;
		return base.OnUpdate();
	}
}
