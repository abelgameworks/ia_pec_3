using UnityEngine;

using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using Pada1.BBCore.Framework;
using System.Collections.Generic;

[Action("MyActions/Thief/FindTarget")]
[Help("Finds a GameObject to perform the steal")]
public class FindClosestTarget : BasePrimitiveAction
{
    [InParam("Self")]
    [Help("Reference to the own GameObject")]
    public GameObject self;

    [InParam("Tag")]
    [Help("Tag of the GameObjects eligibles to be stolen from")]
    public string tag;

    [InParam("Closest")]
    [Help("Select search mode by closest(true) or random(false)")]
    private bool closest;

    [OutParam("Target")]
    [Help("Target of the steal")]
    private GameObject target;

    [OutParam("targetFound")]
    [Help("Does the thief have a target?")]
    private bool targetFound;

    private List<GameObject> entities;

    public override TaskStatus OnUpdate()
    {
        if(entities == null)
        {
            entities = new List<GameObject>();
            if(tag != null)
                entities.AddRange(GameObject.FindGameObjectsWithTag(tag));
        }

        if (closest)
            target = FindClosest(entities);
        else
            target = GetRandom(entities);

        targetFound = true;
        Debug.Log("Target is " + target);
        return base.OnUpdate();
    }

    private GameObject GetRandom(List<GameObject> gos)
    {
        return gos[Random.Range(0,gos.Count)];
    }

    private GameObject FindClosest(List<GameObject> gos)
    {
        Vector3 pos = self.transform.position;
        float dist = float.PositiveInfinity;
        GameObject closest = null;

        foreach(GameObject go in gos)
        {
            var d = (pos - go.transform.position).sqrMagnitude;
            if(d < dist)
            {
                closest = go;
                dist = d;
            }

        }
        this.closest = false;
        return closest;
    }
}
