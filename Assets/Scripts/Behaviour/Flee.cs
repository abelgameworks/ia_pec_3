using UnityEngine;
using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using Pada1.BBCore.Framework;

[Action("MyActions/Thief/Flee")]
[Help("Flees back home")]
public class Flee : BasePrimitiveAction
{
    [InParam("PoliceTarget")]
    [Help("Reference to the Police GameObject")]
    private GameObject target;

    [InParam("Self")]
    [Help("Reference to the own GameObject")]
    private GameObject self;

    [InParam("closeEnough")]
    [Help("Is the target close enough to steal from him?")]
    private bool closeEnough;

    [InParam("targetFound")]
    [Help("Does the thief have a target?")]
    private bool targetFound;

	[InParam("StealTarget")]
    [Help("GameObject chosen to be stolen from")]
    private GameObject StealTarget;

    [OutParam("Fleeing")]
    [Help("Is the thief fleeing from the police?")]
    private bool fleeing;

    // Resets the vars checked to perform a steal so it has to start again the process
    // also sets the thief as fleeing
    public override TaskStatus OnUpdate()
    {
        ResetVars();
        fleeing = true;
        return base.OnUpdate();
    }

    private void ResetVars()
    {
		closeEnough = false;
		targetFound = false;
		StealTarget = null;
	}
}
