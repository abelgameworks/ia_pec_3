using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Pada1.BBCore;
using Pada1.BBCore.Tasks;
using Pada1.BBCore.Framework;

/// <summary>
/// It is an action to "catch" the thief and make it go away, starting also the respawning process.
/// </summary>
[Action("MyActions/Policeman/Catch")]
[Help("Catches the targeted Thief")]
public class Catch : BasePrimitiveAction
{
	[InParam("TargetThief")]
    [Help("Thief currently targeted by this policeman")]
	public GameObject TargetThief;

	[InParam("ThiefManager")]
    [Help("Reference to the Thief Manager GameObject")]
	public ThiefManager ThiefManager;
	public override TaskStatus OnUpdate()
	{
		ThiefManager.Catch();
		TargetThief = null;
		return base.OnUpdate();
	}
}