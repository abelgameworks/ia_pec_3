using Pada1.BBCore.Framework;
using Pada1.BBCore;

namespace BBCore.Conditions
{
    /// <summary>
    /// It is a basic condition to check if a set of two Booleans have the wanted value.
    /// </summary>
    [Condition("MyActions/Conditions/CheckBoolAndBool")]
    [Help("Checks whether two booleans are true")]
    public class CheckBoolAndBool : ConditionBase
    {
        ///<value>Input First Boolean Parameter.</value>
        [InParam("valueA")]
        [Help("First value to be compared")]
        public bool valueA;

        ///<value>Input First Boolean Parameter.</value>
        [InParam("valueA_comparison")]
        [Help("First value to be compared")]
        public bool valueA_comp;

        ///<value>Input Second Boolean Parameter.</value>
        [InParam("valueB")]
        [Help("Second value to be compared")]
        public bool valueB;

        ///<value>Input First Boolean Parameter.</value>
        [InParam("valueB_comparison")]
        [Help("First value to be compared")]
        public bool valueB_comp;

        /// <summary>
        /// Checks whether two booleans have the same value.
        /// </summary>
        /// <returns>the value of compare first boolean with the second boolean.</returns>
		public override bool Check()
        {
            return (valueA == valueA_comp) && (valueB == valueB_comp);
        }
    }
}
