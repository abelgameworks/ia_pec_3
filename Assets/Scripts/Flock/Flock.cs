using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Scripts that manages each flock member based on the 3 flocking rules
/// </summary>
public class Flock : MonoBehaviour
{
    public FlockManager Manager; // Reference to the manager
    public float speed; // Speed at which the flock will move
    public Vector3 direction; // Direction where the flock is headed

    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(Manager.minSpeed, Manager.maxSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        direction = ApplyRules(); 

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction),
            Manager.RotationSpeed * Time.deltaTime);

        transform.Translate(0.0f, 0.0f, Time.deltaTime * speed);
    }

    // Gets a vector of displacement based on the Alignment, separation and cohesion rules
    Vector3 ApplyRules()
    {
        Vector3 Align = Vector3.zero; ; 
        Vector3 Separation = Vector3.zero; ;
        Vector3 Cohesion = Vector3.zero;
        int num = 0;

        foreach(GameObject go in Manager.AllBugs)
        {
            if(go != this.gameObject)
            {
                float dist = Vector3.Distance(go.transform.position, transform.position);

                if(dist <= Manager.NeighbourDistance)
                {
                    Separation -= (transform.position - go.transform.position) / (dist * dist);
                    Align += go.GetComponent<Flock>().direction;
                    Cohesion += go.transform.position;
                    num++;
                }
            }
        }

        if (num > 0)
        {
            Align /= num;
            speed = Mathf.Clamp(Align.magnitude, Manager.minSpeed, Manager.maxSpeed);
            Cohesion = (Cohesion / num - transform.position).normalized * speed;
        }

        return (Cohesion + Align + Separation).normalized * speed;
    }
}
