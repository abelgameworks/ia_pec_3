using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that instantiates a Flock
/// </summary>
public class FlockManager : MonoBehaviour
{
    [Header("Swarm Basics")]
    public GameObject BugPrefab; // Reference to the Bug Prefab GameObject
    public int NumBugs; // Number of flocks to be instantiated
    public GameObject[] AllBugs; // Array containing the instantiated flocks
    public Vector3 SwarmLimits = new(5,0.5f,5); // Area to limit the spawn of the flock
    
    [Header("Swarm Settings")]
    [Range(0.0f, 5.0f)]
    public float maxSpeed; // Maximum speed value
    [Range(0.0f, 5.0f)]
    public float minSpeed; // Minimum speed value
    [Range(1.0f, 10.0f)]
    public float NeighbourDistance; // Distance to check for Neighbours
    [Range(0.0f, 5.0f)]
    public float RotationSpeed; // Speed to rotate

    private float speed;

    // Start is called before the first frame update
    void Start()
    {
        AllBugs = new GameObject[NumBugs];
        for (int i = 0; i < NumBugs; ++i)
        {
            Vector3 pos = this.transform.position + getBoundaries();
            AllBugs[i] = (GameObject)Instantiate(BugPrefab, pos, Quaternion.identity);
            AllBugs[i].GetComponent<Flock>().Manager = this;
            AllBugs[i].transform.parent = this.gameObject.transform;
        }

        speed = Random.Range(minSpeed, maxSpeed);
    }

    // Gets a random vector inside the Swarm Limits 
    private Vector3 getBoundaries()
    {
        return new Vector3(
            Random.Range(-SwarmLimits.x, SwarmLimits.x),
            0.5f,
            Random.Range(-SwarmLimits.z, SwarmLimits.z)
            );
    }

    private void Update()
    {
        if (transform.position.x < 7)
        {
            if(transform.position.z < 7)
            {
                transform.Translate(Time.deltaTime * speed, 0.0f, 0.0f);
            } else
            {
                transform.Translate(Time.deltaTime * speed*-1, 0.0f, 0.0f);
            }
        }
        if (transform.position.x >= 7)
        {
            if(transform.position.z < 8.5)
            {
                transform.Translate(0.0f, 0.0f, Time.deltaTime * speed);
            }
            else
            {
                transform.Translate(0.0f, 0.0f, Time.deltaTime * speed*-1);
            }
        }
    }
}
