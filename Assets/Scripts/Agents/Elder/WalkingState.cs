using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class that implements the interface IStateElder and the WalkingState for the Elder
public class WalkingState : IStateElder
{
    private readonly ElderStateMachine esm; // Reference to the ElderStateMachine that uses this state instace
    [SerializeField]
    public float secondsUntilTired; // Seconds walking until the Elder gets tired
    private float targetTime; // Time left to get tired

    // Public constructor, needs an ElderStateMachine to manage it. Gets a random amount a time needed to rest from a range given by the Machine
    public WalkingState(ElderStateMachine esm)
    {
        this.esm = esm;
        secondsUntilTired = Random.Range(esm.minTired, esm.maxTired);
        targetTime = secondsUntilTired;
    }

    // Update method
    public void UpdateState()
    {
        //Reduces the time left to walk
        targetTime -= Time.deltaTime;

        // Checks if the elder is tired
        if (targetTime <= 0.0f)
        {
            esm.tired = true;
        }

        // Checks if the Elder reached its destination
        if (Vector3.Distance(esm.gameObject.transform.position, esm.destination.transform.position) < 2)
        {
            getNextPoint(); // Gets next point where the elder should go
        }
    }

    // Changes to RestingState
    public void ToRestingState()
    {
        esm.actualState = esm.restingState;
    }

    // Not really used since the elder is already in that state
    public void ToWalkingState()
    {
        Debug.Log("Already Walking");
    }

    // Gets the next point of the wanderable area where the elder should go based on if he's going or returning
    private void getNextPoint()
    {

        if (!esm.returning)
            esm.nextDestination++;
        else
            esm.nextDestination--;

        esm.destination = esm.wanderableArea.getNextPoint(esm.nextDestination); // Stores the point in other variable to not lose it if the Elder rests
        esm.agent.SetDestination(esm.destination.position); // Starts going to the next point

        // Checks if the Elder reached one of the route limits
        if (esm.nextDestination >= 3)
        {
            esm.nextDestination = 1;
            esm.returning = true;
        }
        else if (esm.nextDestination < 0)
        {
            esm.nextDestination = 1;
            esm.returning = false;
        }
    }
}
