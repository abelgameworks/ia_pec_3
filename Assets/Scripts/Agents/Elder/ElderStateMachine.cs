using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Class that manages the State of the Elder using different states
public class ElderStateMachine : MonoBehaviour
{
    public NavMeshAgent agent; // reference to the agent that allows the object to move through the navmesh
    public Transform destination; // point where the elder is going
    public float walkingSpeed;
    public bool returning = false; // is the elder going from the entrance to the exit or viceversa?
    public bool tired; // is the elder tired of walking?

    public float minRest; // minimun resting time
    public float maxRest; // maximum resting time

    public float minTired; // minimun time until tired
    public float maxTired; // maximum time until tired

    [HideInInspector] public IStateElder actualState; // current state of the state machine
    [HideInInspector] public WalkingState walkingState; // walking sate used by the state machine
    [HideInInspector] public RestingState restingState; // resting sate used by the state machine

    public WanderableArea wanderableArea; // script that defines the points where the elder will go
    public int nextDestination; // index of the next position where the elder is going

    private void Awake()
    {
        walkingState = new WalkingState(this);
        restingState = new RestingState(this);

        agent = GetComponent<NavMeshAgent>();
        destination = wanderableArea.getNextPoint(nextDestination); // gets the next point
        agent.SetDestination(destination.position); // makes the point it got as destination
    }

    // Start is called before the first frame update
    void Start()
    {
        actualState = walkingState;
    }

    // Update is called once per frame
    void Update()
    {
        actualState.UpdateState();

    }

    // method to change the state from walking to resting
    public void goRest(Bench bench, int spot)
    {
        restingState.occupiedBench = bench; // reference to the bench where the elder will rest
        restingState.occupiedBenchSpot = spot; // spot of the bench where the elder will sit
        actualState.ToRestingState();
    }
}
