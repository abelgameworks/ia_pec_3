using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class that implements the interface IStateElder and the RestingState for the Elder
public class RestingState : IStateElder
{
    private readonly ElderStateMachine esm; // Reference to the ElderStateMachine that uses this state instace

    public Bench occupiedBench = null; // Which bench is occupying if any
    public int occupiedBenchSpot = 0; // spot occupied in the bench
    [SerializeField]
    public float secondsToRest = 0; // Seconds that have to pass so the agent isn't tired any longer
    private float targetTime; // Variable that counts how much has the elder left to rest

    // Public constructor, needs an ElderStateMachine to manage it. Gets a random amount a time needed to rest from a range given by the Machine
    public RestingState(ElderStateMachine esm)
    {
        this.esm = esm;
        if(secondsToRest == 0)
            secondsToRest = Random.Range(esm.minRest, esm.maxRest);
        targetTime = secondsToRest;
    }

    // Update Method
    public void UpdateState()
    {
        // Adds time
        targetTime -= Time.deltaTime;

        // Checks if the elder rested enough
        if(targetTime <= 0.0f)
        {
            ToWalkingState(); // Makes the elder walk again
        }
    }

    // Not really used since the elder is already in that state
    public void ToRestingState()
    {
        Debug.Log("Already resting");
    }

    // Restarts the variables needed when the Elder needs to rest again and changes its state to the walking one
    public void ToWalkingState()
    {
        esm.tired = false;
        targetTime = secondsToRest;
        esm.agent.SetDestination(esm.destination.position);
        esm.actualState = esm.walkingState;
    }

}
