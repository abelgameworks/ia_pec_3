using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Common interface to implement by the different States
public interface IStateElder
{
    // Update method
    void UpdateState();

    // Change to WalkingState
    void ToWalkingState();

    // Change to RestingState
    void ToRestingState();
}
