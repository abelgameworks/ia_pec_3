using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Script to start a formation
/// </summary>
public class Slots : MonoBehaviour
{
    public int students; // Number of units in the formation
    public GameObject studentPrefab; // Reference to the student prefab
    public GameObject teacher; // Reference to the leader of the formation

    private void Start()
    {
        int front = students / 2; // Divide the formation in 2 rows
        int rear = students - front; // Calculate the second row size
        CreateRow(front, -3f, studentPrefab); // Create front row
        CreateRow(rear, -5f, studentPrefab); // Create back row
        teacher.GetComponent<TaiChiInstructor>().enabled = true; // Starts the thai chi choreographies
    }

    // Creates a Formation row
    void CreateRow(int num, float z, GameObject prefab)
    {
        float pos = 1 - num;
        for(int i = 0; i < num; ++i)
        {
            Vector3 position = teacher.transform.TransformPoint(new Vector3(pos, 0f, z));
            GameObject temp = (GameObject)Instantiate(prefab, position, teacher.transform.rotation);
            temp.AddComponent<Formation>();
            temp.GetComponent<Formation>().pos = new Vector3(pos, 0, z);
            temp.GetComponent<Formation>().target = teacher;
            pos += 2f;
        }
    }
}
