using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Script that performs the thai chi choreography in a random way
/// </summary>
public class TaiChiInstructor : MonoBehaviour
{
    NavMeshAgent agent;

    // Enum defined to randomize the choreography
    private enum ChoreographyStep { x_move, z_move, rotation, none }
    private ChoreographyStep currentStep = ChoreographyStep.none; // Step of the choreography that's being performed
    private bool stepFinished = false; // Has the step finished?
    private bool destinationAssigned = false; // Is the instructor currently moving?
    private Transform startingPosition; // Reference to the GameObject starting position

    private int xOffset; // Displacement in the x axis from the starting position
    private int zOffset; // Displacement in the z axis from the starting position

    public float timeBetweenActions = 2f; // Time between steps of the choreography
    private float timer = 0f; 

    // Start is called before the first frame update
    void Start()
    {
        startingPosition = this.transform;
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        currentStep = (ChoreographyStep)2; // Start by rotating
    }

    // Update is called once per frame
    void Update()
    {
        // if the current step is finished, select a new one. If not, continue performing
        if (stepFinished)
        {
            currentStep = (ChoreographyStep)PickAMove(); // Gets a new random choreography step
        }
        else 
        {
            PerformStep(); // Performs the choreography step
        }

    }

    // Picks a random move from the available ones, waits until the timer is full in order to perform
    private int PickAMove()
    {
        timer += Time.deltaTime;
        if(timer >= timeBetweenActions)
        {
            stepFinished = false;
            timer = 0f;
            return Random.Range(0, 3);
        }
        return (int)ChoreographyStep.none;
    }

    // Performs the step actions based on the current one
    private void PerformStep()
    {
        if (currentStep == ChoreographyStep.rotation)
        {
            Rotate();
        }
        else if (!destinationAssigned) // if the leader is not moving get the new moving destination
        { 
            if (currentStep == ChoreographyStep.x_move)
            {
                MoveX();
            }
            if (currentStep == ChoreographyStep.z_move)
            {
                MoveZ();
            }
        } else // if the leader has a target destination, just check if it's close enough to it
        {
            if ((gameObject.transform.position - agent.destination).sqrMagnitude == 1)
            {
                destinationAssigned = false;
                stepFinished = true;
            }
        }
    }

    // Performs the movement in the X axxis basing in the offset so it won't exceed the area used by the formation
    private void MoveX()
    {
        // If the offset is 0, select a random direction. If it isn't, go back to the original value for that axxis
        if (xOffset == 0)
        {
            if (Random.Range(1, 2) % 2 == 0)
                xOffset = 1;
            else
                xOffset = -1;
        }
        else
        {
            xOffset *= -1;
        }

        agent.destination = new Vector3(gameObject.transform.position.x + xOffset, gameObject.transform.position.y, gameObject.transform.position.z); // set the new destination by adding the new offset

        destinationAssigned = true; // mark the leader as moving

        if (agent.destination == startingPosition.position) // if the leader is in its starting position reset the offset
            xOffset = 0;
    }

    // Performs the movement in the Y axxis basing in the offset so it won't exceed the area used by the formation
    private void MoveZ()
    {
        // If the offset is 0, select a random direction. If it isn't, go back to the original value for that axxis
        if (zOffset == 0)
        {
            if (Random.Range(1, 2) % 2 == 0)
                zOffset = 1;
            else
                zOffset = -1;
        }
        else
        {
            zOffset *= -1;
        }

        agent.destination = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + zOffset);

        destinationAssigned = true; // mark the leader as moving

        if (agent.destination == startingPosition.position) // if the leader is in its starting position reset the offset
            zOffset = 0;
    }

    // Performs a 360 degrees rotation
    private void Rotate()
    {
        if (gameObject.transform.rotation.y >= 0 )
        {
            gameObject.transform.Rotate(Vector3.up * 100 * Time.deltaTime, Space.Self);            
        }
        else
        {
            stepFinished = true;
        }
    }
}
